package com.example.springhw002.service.serviceImple;

import com.example.springhw002.model.Article;
import com.example.springhw002.repository.ArticleRepository;
import com.example.springhw002.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class ArticleServicesImple implements ArticleService {

    @Autowired
    private final ArticleRepository articleRepository;

    @Override
    public List<Article> getAllArticle() {
        return articleRepository.getAllArticle();
    }

    @Override
    public void addNewArticle(Article article) {
        articleRepository.addNewArticle(article);
    }

    @Override
    public Article findArticleByid(int id) {
        return articleRepository.findArticleByid(id);
    }


    public void deleteById(int id) {
        articleRepository.deleteArticleByid(id);
    }

}
