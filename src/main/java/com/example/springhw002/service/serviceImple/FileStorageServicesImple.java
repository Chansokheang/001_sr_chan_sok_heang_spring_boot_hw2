package com.example.springhw002.service.serviceImple;
import com.example.springhw002.service.FileStorageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageServicesImple implements FileStorageService {
    Path fileStorageLocation;

    FileStorageServicesImple(){
        fileStorageLocation= Paths.get("src/main/resources/images");
    }

    @Override
    public String saveFile(MultipartFile file) throws IOException {

        String filename = file.getOriginalFilename();

        if (filename.contains("..")){
            System.out.println("Wrong format of the file.");
            return null;
        }
        String[] fileParts =filename.split("\\.");
        //extension
        filename = UUID.randomUUID() + "."+ fileParts[1];
        Path resolvedPath = fileStorageLocation.resolve(filename);
        Files.copy(file.getInputStream(),resolvedPath, StandardCopyOption.REPLACE_EXISTING);

        return filename;
    }
}
