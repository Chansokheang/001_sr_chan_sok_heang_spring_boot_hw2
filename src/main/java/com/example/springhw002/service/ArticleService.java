package com.example.springhw002.service;

import com.example.springhw002.model.Article;

import java.util.List;

public interface ArticleService {
    public List<Article> getAllArticle();
    public void addNewArticle(Article article);
    public Article findArticleByid(int id);

    void deleteById(int id);
}
