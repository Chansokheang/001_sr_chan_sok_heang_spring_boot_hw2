package com.example.springhw002.repository;

import com.example.springhw002.model.Article;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


@Repository
public class ArticleRepository {

    List<Article> list = new ArrayList<>();

    Article article = new Article();

   ArticleRepository(){
       Article article1 = new Article();
       Article article2 = new Article();
       Article article3= new Article();

            article1.setId(1);
            article1.setTitle("Spring");
            article1.setDescription("Spring Framework is a Java platform that provides comprehensive infrastructure support for developing Java applications. Spring handles the infrastructure so you can focus on your application.");
            article1.setImage("http://localhost:8080/images/Spring.png");
            list.add(article1);

       article2.setId(2);
       article2.setTitle("Docker");
       article2.setDescription("Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. " +
               "With Docker, you can manage your infrastructure in the same ways you manage your applications. " +
               "By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.");
       article2.setImage("http://localhost:8080/images/fe6af59b-24fd-412c-a603-f8609bcbf14d.png");
       list.add(article2);

       article3.setId(3);
       article3.setTitle("Linux");
       article3.setDescription("Linux is a family of free and open-source operating systems based on the Linux kernel. Operating systems based on Linux are known as Linux distributions or distros. " +
               "Examples include Debian, Ubuntu, Fedora, CentOS, Gentoo, Arch Linux, and many others.");
       article3.setImage("http://localhost:8080/images/c7b8113247fecd83bd9b5ed5bd3f34d5.png");
       list.add(article3);
    }



    public List<Article> getAllArticle(){
        return list;
    }

    public void addNewArticle(Article article){
        article.setId(list.size()+1);
        list.add(article);
    }

    public Article findArticleByid(int id){
        return list.stream().filter(article -> article.getId()==id).findFirst().orElseThrow();
    }

    public void deleteArticleByid(int id){
       for(int i=0; i<list.size(); i++)
       {
           if(list.get(i).getId()==id){
               list.remove(i);
               break;
           }
       }
    }
    public void update(int id,Article article){
        for(int i=0; i<list.size(); i++)
        {
            if(list.get(i).getId()==article.getId()){
                list.set(i, article);
                break;
            }
        }
    }

}
