package com.example.springhw002.model;


import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Article {

    private int id;

    @NotEmpty(message = "Please provide a title.")
    private String title;
    @NotEmpty(message = "Please provide a description for your article.")
    private String description;

    private String image;
    private MultipartFile file;

}
