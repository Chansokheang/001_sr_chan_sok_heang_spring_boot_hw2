package com.example.springhw002.controller;

import com.example.springhw002.model.Article;
import com.example.springhw002.repository.ArticleRepository;
import com.example.springhw002.service.ArticleService;
import com.example.springhw002.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Controller
public class ArticleController {


    @Autowired
    @Qualifier("articleServicesImple")
    ArticleService articleService;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    FileStorageService fileStorageService;

    Locale locale = LocaleContextHolder.getLocale();

    @RequestMapping("/article")
    public String index(Model model){
        model.addAttribute("article", new Article());
        model.addAttribute("articles", articleService.getAllArticle());
        return "index";
    }
    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Article article, BindingResult bindingResult){

        if (article.getFile().isEmpty()){
            // set default profileImage ;
            String s = "http://localhost:8080/images/noImage.jpg";
            article.setImage(s);
        }
        else {
            try{
                String filename = "http://localhost:8080/images/"+fileStorageService.saveFile(article.getFile());
                article.setImage(filename);
            }catch (IOException ex){
                System.out.println("Error with the image upload "+ex.getMessage());
            }
        }
        if (bindingResult.hasErrors()){
            return "/form-add";
        }
        articleService.addNewArticle(article);
        //  return "index";
        return  "redirect:/article";
    }


    @GetMapping("/form-add")
    public String showFormAdd(Model model){
        model.addAttribute("article", new Article());
        return "form-add";
    }


    @GetMapping("/view/{id}")
    public String viewArticle(@PathVariable int id,Model model){
        // Find Student By ID
        Article resultStudent = articleService.findArticleByid(id);
        model.addAttribute("article",resultStudent);
        return "view";
    }

    @GetMapping(value = "/delete/{id}")
    public String deleteArticle(@PathVariable int id){
//        articleService.deleteById(id);
        articleRepository.deleteArticleByid(id);

        return "redirect:/article";
    }


    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable int id, Model model){
        Article i = articleRepository.findArticleByid(id);
        model.addAttribute("article",i);
        model.addAttribute("heading","Edit Item");

        return "update-form";
    }
    @PostMapping("/handle-update/{id}")
    public String edit(@PathVariable int id,@ModelAttribute @Valid Article article, BindingResult bindingResult){
        Article result = articleRepository.findArticleByid(id);
        if (article.getFile().isEmpty()){;
            article.setImage(result.getImage());
        }
        else {
            try{
                String filename = "http://localhost:8080/images/"+fileStorageService.saveFile(article.getFile());
                article.setImage(filename);
            }catch (IOException ex){
                System.out.println("Error with the image upload "+ex.getMessage());
            }
        }
        if (bindingResult.hasErrors()){
            return "update-form";
        }
        articleRepository.update(id, article);
        return "redirect:/article";
    }
}
